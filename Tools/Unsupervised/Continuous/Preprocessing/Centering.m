function [Xout,Center] = Centering(Xin,Center,Mode,Method)

% -------------------------------------------------------------------------
% DM Toolbox - Centering.m
% -------------------------------------------------------------------------
% Description 
%
% This function identifies or applies a model center (eg, mean centering).
% The function provides three modes of operation, which are:
%   0	-   Calibration mode
%   +1	-   Projection mode
%   -1	-   Reconstruction mode
% The function currently supports the use of multiple methods to identify a
% reference point: 
%   0       -   No centering
%   1, 11   -   Slab-wise mean (overall 1st and 2nd dimension)
%   2, 12   -   Slab-wise median (overall 1st and 2nd dimension)
%   3, 13   -   Slab-wise center or range (overall 1st and 2nd dimension)
%   4, 21   -   Column-wise mean (overall 1st and 2nd dimension)
%   5, 22   -   Column-wise median (overall 1st and 2nd dimension)
%   6, 23   -   Column-wise center or range (overall 1st and 2nd dimension)
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Xout,center] = Centering(Xin,center,mode,method)
%
% INPUT
%	Xin         Input data. These can be data to center or to reconstruct.
%               (required)
%	Center      Existing center values
%               (ignored for mode 0, required for modes +1 and -1)
%	Mode        Integer indicating the mode of operation (see above).
%               (optional, default=0)
%	Method      Applied centering method
%               (optional, default=0)
%
% OUTPUT
%	Xout     	Centered data (Mode = 0 or +1) or
%               reconstructed data (Mode = -1)
%   Center      Center values
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% mode:
%	1: forward (projection)
%	0: estimation
% 	-1: reverse (reconstruction)

 [m,n] = size(Xin) ;
 
if nargin<2 || isempty(Center)
	if nargin>=3 && ~isempty(Mode)
		assert(Mode==0,'Improper mode of operation')
	else
		mode = 0 ;
	end
end

if nargin==2 || isempty(Mode)
	Mode = 1 ; % forward
end

if Mode==0 % estimation
	if nargin<4 || isempty(Method)
		Method = 11;
	end
	
	if iscell(Method)
		Method = Method{1} ;
	end
	
	switch	Method
		case {0,'none'} 
			Center = zeros(1,n) ;
		case {1,11,'slab-mean'}
			Center = mean(Xin(:))*ones(1,n) ; % overall mean
		case {2,12,'slab-median'}
			Center = median(Xin(:))*ones(1,n) ; % overall median
		case {3,13,'slab-midrange'}
			Center = (max(Xin(:))+min(Xin(:)))/2*ones(1,n) ; % overall middle of range
		case {4,21,'column-mean'}
			Center = mean(Xin,1) ; % column-wise mean
		case {5,22,'column-median'}
			Center = median(Xin,1) ; % column-wise median
		case {6,23,'column-midrange'}
			Center = (max(Xin,1)+min(Xin,1))/2 ; % column-wise moddle of range
		otherwise
			error('unknown Centering method')
	end
end

assert(1==size(Center,1),'Dimensions of Center vector are invalid (should be row vector)')
assert(n==size(Center,2),'Dimensions of data matrix and Center vector do not match')

switch Mode
	case {0,1} % estimation/projection
		[m] = size(Xin,1) ;
		Xout 	=	Xin - repmat(Center,[m 1]) ;
	case -1 % reconstruction
	[m] = size(Xin,1) ;
		Xout 	=	Xin + repmat(Center,[m 1]) ;
end
