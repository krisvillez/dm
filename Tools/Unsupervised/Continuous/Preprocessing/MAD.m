function Xout = MAD(Xin,Mode) 

% -------------------------------------------------------------------------
% DM Toolbox - Xout.m
% -------------------------------------------------------------------------
% Description 
%
% This function identifies the average absolute deviation (from zero). The
% function provides three modes of operation, which are: 
%   0	-   Multiplied by n/(n-1)
%   1	-   Standard definition
%			
% -------------------------------------------------------------------------
% SyntaXin
%
% I/O:	Xout = MAD(Xin,Mode) 
%
% INPUT
%	Xin     Input data.
%           (required)
%	Mode	Divide by n-1 if 0, n if 1
%           (optional, default=0)
%
% OUTPUT
%	Xout	Median Absolute Deviation
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin < 2 || isempty(Mode)
	Mode = 0 ;
end

[m,n]   =   size(Xin)         ;
Xout	=	median(abs(Xin))  ;

switch Mode
	case 0 
		Xout = Xout*m/(m-1) ;
	case 1
		% no correction
	otherwise
		error('Unknown Mode in ''mad.m''.')
end
