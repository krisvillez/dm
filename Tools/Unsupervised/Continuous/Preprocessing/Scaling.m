function [Xout,Scale] = Scaling(Xin,Scale,Mode,Method)

% -------------------------------------------------------------------------
% DM Toolbox - Scaling.m
% -------------------------------------------------------------------------
% Description 
%
% This function identifies or applies data scaling parameters (eg, unit
% variance scaling). The function provides three modes of operation, which
% are: 
%   0	-   Calibration mode
%   +1	-   Projection mode
%   -1	-   Reconstruction mode
% The function currently supports the use of multiple methods to identify a
% reference point: 
%   0       -   No scaling
%   1, 110  -   Slab-wise unbiased standard deviation
%   2, 111  -   Slab-wise maximum likelihood standard deviation
%   3, 120  -   Slab-wise average absolute deviation (divided by n-1)
%   4, 121  -   Slab-wise average absolute deviation (divided by n)
%   5, 130  -   Slab-wise median absolute deviation (divided by n-1)
%   6, 131  -   Slab-wise median absolute deviation (divided by n)
%   7, 140  -   Slab-wise data range divided by two 
%   8,	210	-   Column-wise unbiased standard deviation
%   9,	211	-   Column-wise maximum likelihood standard deviation
%   10, 220	-   Column-wise average absolute deviation (divided by n-1)
%   11, 221	-   Column-wise average absolute deviation (divided by n)
%   12, 230	-   Column-wise median absolute deviation (divided by n-1)
%   13, 231	-   Column-wise median absolute deviation (divided by n)
%   14, 240	-   Column-wise data range divided by two 
%			
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[Xout,Scale] = Scaling(Xin,Scale,Mode,Method)
%
% INPUT
%	Xin         Input data. These can be data to center or to reconstruct.
%               (required)
%	Scale      Existing center values
%               (ignored for mode 0, required for modes +1 and -1)
%	Mode        Integer indicating the mode of operation (see above).
%               (optional, default=0)
%	Method      Applied centering method
%               (optional, default=0)
%
% OUTPUT
%	Xout     	Centered data (Mode = 0 or +1) or
%               reconstructed data (Mode = -1)
%   Center      Center values
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-07-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2015-2016 Kris Villez
%
% This file is part of the DM Toolbox for Matlab/Octave. 
% 
% The DM Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The DM Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the DM Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% mode:
%	1: forward (projection)
%	0: estimation
% 	-1: reverse (reconstruction)

 [m,n] = size(Xin) ;

if nargin<2 || isempty(Scale)
	if nargin>=3 && ~isempty(Mode)
		assert(Mode==0,'Improper Mode of operation')
	else
		Mode = 0 ;
	end
end

if nargin==2 || isempty(Mode)
	Mode = 1 ; % forward
end

if Mode==0 % estimation
	if nargin<4 || isempty(Method)
		Method = 0;
	end
	
	if iscell(Method)
		Method = Method{1} ;
	end
	switch	Method
		case {0,'none'}
			Scale = ones(1,n) ; % no scaling
		case {1,110,'slab-std-unbiased'} % 1- std, 0- overall scaling, 0- unbiased estimator
			Scale = std(Xin(:),0)*ones(1,n) ; 
		case {2,111,'slab-std-maxlik'} % 1- std, 0- overall scaling, 1- max likelihood estimator
			Scale = std(Xin(:),1)*ones(1,n) ; 
		case {3,120,'slab-AAD-unbiased'} % 2- MAD, 0- overall scaling, 0- unbiased estimator
			Scale = AAD(Xin(:),0)*ones(1,n) ; 
		case {4,121,'slab-AAD-maxlik'} % 2- MAD, 0- overall scaling, 1- max likelihood estimator
			Scale = AAD(Xin(:),1)*ones(1,n) ; 
		case {5,130,'slab-MAD-unbiased'} % 2- MAD, 0- overall scaling, 0- unbiased estimator
			Scale = MAD(Xin(:),0)*ones(1,n) ; 
		case {6,131,'slab-MAD-maxlik'} % 2- MAD, 0- overall scaling, 1- max likelihood estimator
			Scale = MAD(Xin(:),1)*ones(1,n) ; 
		case {7,140,'slab-range'} % 2- MAD, 0- overall scaling, 1- max likelihood estimator
			Scale = range(Xin(:))*ones(1,n)/2 ;
		case {8,210, 'column-std-unbiased'} % 1- std, 1- column-wise scaling, 0- unbiased estimator
			Scale = std(Xin,0) ; 
		case {9,211, 'column-std-maxlik'} 
			Scale = std(Xin,1) ; 
		case {10,220, 'column-AAD-unbiased'} % 2- MAD, 1- column-wise scaling, 0- unbiased estimator
			Scale = AAD(Xin,0) ; 
		case {11,221, 'column-AAD-maxlik'} % 2- MAD, 1- column-wise scaling, 1- max likelihood estimator
			Scale = AAD(Xin,1) ; 
		case {12,230, 'column-MAD-unbiased'} % 2- MAD, 1- column-wise scaling, 0- unbiased estimator
			Scale = MAD(Xin,0) ; 
		case {13,231, 'column-MAD-maxlik'} % 2- MAD, 1- column-wise scaling, 1- max likelihood estimator
			Scale = MAD(Xin,1) ; 
		case {14,240, 'column-range'}  % 3- min-max scaling, 1- column-wise scaling
			Scale = range(Xin)/2  ;
		otherwise
			Method
			error('Unknown scaling Method')
	end
end

assert(1==size(Scale,1),'Dimensions of Scale vector are invalid (should be row vector)')
assert(n==size(Scale,2),'Dimensions of data matrix and Scale vector do not match')

switch Mode
	case {0,1} % estimation/projection
		[m] = size(Xin,1) ;
		Xout 	=	Xin ./ repmat(Scale,[m 1]) ;
	case -1 % reconstruction
	[m] = size(Xin,1) ;
		Xout 	=	Xin .* repmat(Scale,[m 1]) ;
end
