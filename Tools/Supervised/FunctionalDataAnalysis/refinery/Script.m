
clc
clear all
close all

load refinery

figure
    han(1) = subplot(2,1,1) ;
        plot(Time,Reflux,'.-')
    han(2) = subplot(2,1,2) ; 
        plot(Time,Tray47,'.-')

ArgumentRange = [Time(1) Time(end)]         ;
Knots = [Time(1):2:Time(end-1) Time(end)]   ;
Knots = sort(Knots ) ;

basis = create_bspline_basis(ArgumentRange,[],4,Knots) ;

Time_eval   = Time(1):.1:Time(end)          ;
B           = eval_basis(Time_eval,basis)   ;

Time_cal    = [Time(1):2:Time(end-2) Time(end-1)]   ;
Time_val    = [Time(2):2:Time(end-2) ]   ;
Bcal          = eval_basis(Time_cal,basis)   ;
Bval          = eval_basis(Time_val,basis)   ;
 
figure, 
    hold on
    plot(Time_eval,B,'-')
    plot(Time_cal,Bcal,'.')
    plot(Time_val,Bval,'.')

Y   =   [ Reflux Tray47 ];
Ycal = Y(1:2:end,:);
Yval = Y(2:2:end-1,:);
Phi =   Bcal      ;

    R=eval_penalty(basis,1) ;

for j=0:100

    lambda = 10^(j/10-5)         ;
    LAMBDA(j+1) = lambda ;

    c   =   (Phi'*Phi+lambda*R)^(-1)*Phi'*Ycal ;

    H = Phi*(Phi'*Phi+lambda*R)^(-1)*Phi' ;
    df(j+1) = trace(H) ;

    yhat_cal = Bcal*c ;
    yhat_val = Bval*c ;

%     axes(han(1)) 
%     hold on
%     plot(Time_cal,yhat_cal(:,1),'k.')
%     plot(Time_val,yhat_val(:,1),'r.')
%     %plot(Time_eval,yhat(:,1),'r-')
%     axes(han(2)) 
%     hold on
%     plot(Time_cal,yhat_cal(:,2),'k.')
%     plot(Time_val,yhat_val(:,2),'r.')

    SSRcal(j+1) = sum((Ycal(:,2) - yhat_cal(:,2)).^(2)) ;
    SSRval(j+1) = sum((Yval(:,2) - yhat_val(:,2)).^(2)) ;
end

n = size(Ycal,1) ;
GCV = (n./(n-df)).*(SSRcal./(n-df)) ;
figure
    subplot(2,1,1)
    hold on
    plot(LAMBDA,SSRcal,'b-')
    plot(LAMBDA,SSRval,'r-')
%     set(gca,'xscale','log','yscale','log') ;
%     subplot(2,1,2)
%     hold on
    plot(LAMBDA,GCV*n,'m-')
    set(gca,'xscale','log','yscale','log') ;


[SSRmin,loc] = min(SSRval) ;
lambda_min = LAMBDA(loc) ;

c   =   (Phi'*Phi+lambda_min*R)^(-1)*Phi'*Ycal ;

    yhat_cal = Bcal*c ;
    yhat_val = Bval*c ;

    axes(han(1)) 
    hold on
    plot(Time_cal,yhat_cal(:,1),'k.')
    plot(Time_val,yhat_val(:,1),'r.')
    %plot(Time_eval,yhat(:,1),'r-')
    axes(han(2)) 
    hold on
    plot(Time_cal,yhat_cal(:,2),'k.')
    plot(Time_val,yhat_val(:,2),'r.')



return
fdo     =   data2fd(Time,[Reflux Tray47],basis)  ;

c       =   getcoef(fdo)                ;
yhat    =   eval_fd(Time_eval,fdo)      ;
 
axes(han(1)) 
hold on
plot(Time_eval,yhat(:,1),'r-')
axes(han(2)) 
hold on
plot(Time_eval,yhat(:,2),'r-')
