DM toolbox v1.0 - Data-driven Modelling toolbox
by Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions and scripts for Data-driven Modelling. It also includes a slightly modified version of the FDA toolbox by Ramsay and Silverman [1]. They are released to the public under the GPL v3 license in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the file LICENSE.TXT located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2014b) and Octave (v3.6.4) on a Windows system. Tests in Octave are incomplete at the time of writing. To install the Matlab functions in a Windows systems, using Matlab or Octave, follow the instructions below.

---------------------------------------------------
I. Installing the toolbox and other preliminaries
---------------------------------------------------

1. Create a directory to contain the toolbox.  For example, in my case all toolbox directories are in:

C:\Tools\

2. Move the unzipped folder (in which this README_SCS.txt file is located) to the following location:

C:\Tools\DM_v1.0\DM\

-----------------------
II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the toolboxes from any other folder, type the following statement in the command line for each toolbox

addpath(genpath('C:\Tools\DM_v1.0\DM\')	)

This allows access during the current Matlab/Octave session.

3. To enable access to the toolboxes permanently (across sessions), type the following in the command line:

savepath

4. To test and demo the SCS toolbox, execute the script 'DemoPCA.m' located in the Demos folder (e.g. C:\Tools\DM_v1.0\DM\ ) by typing the following in the command line:

Demo_PCA

5. For help with the DM toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:

help EstimationPCA

----------------------------
III. Relevant publications
----------------------------

Other relevant publications include:
[1] Ramsay J. O.; Silverman B. W. (2002). Applied functional data analysis: methods and case studies. Springer, New York.

Please refer to this toolbox by citing following publication:
[2] Habermacher, J.; Villez K. (2016). Shape Anomaly Detection for Process Monitoring of a Sequencing Batch Reactor. Computers and Chemical Engineering, Accepted, In Press.

-------------
IV. Changes
-------------

No changes have been recorded

----------------------------------------------------------
Last modified on 11 May 2016